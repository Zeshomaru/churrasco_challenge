import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        stateEmail: '',
        stateCheckbox: false,
        accounts: []
    },
    mutations: {
        setEmail (state, payload) {
            state.stateEmail = payload
        },
        setCheckBox (state, payload) {
            state.stateCheckbox = payload 
        },
        createAccount (state, payload) {
            state.accounts.push(payload)
        },
        clearEmail (state){
            state.stateEmail = ''
        }
    }
});