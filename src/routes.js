import Login from './components/Login.vue';
import Accounts from './components/Accounts.vue';

const routes = [
    { path: '/login', component: Login },
    { path: '/accounts', component: Accounts }
];

export default routes;