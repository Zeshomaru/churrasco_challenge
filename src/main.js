import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueRouter from 'vue-router';
import Vuelidate from 'vuelidate'
import routes from './routes'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store'
 

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(Vuelidate)
Vue.use(VueAxios, axios)

const router = new VueRouter({routes});

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
